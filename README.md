# README #

Use the "develop" branch for the latest code.

Build:
mvn clean compile assembly:single

Run:
java -jar target/org.tacbrd.mapminer2kml-<version>-jar-with-dependencies.jar
