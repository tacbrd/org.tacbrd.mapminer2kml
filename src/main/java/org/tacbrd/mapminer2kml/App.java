package org.tacbrd.mapminer2kml;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Icon;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.Style;

/**
 * Hello world!
 * 
 */
public class App {
	private static final Logger logger = LoggerFactory.getLogger(App.class);
	
	public static String removeBadCharacters(String in) {
		return in.replaceAll("[^\\x20-\\x7e]", "");
	}

	public static void buildKML(String query, String filename) {
		System.out.println("Start buildKML");
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String url = "jdbc:postgresql://localhost:5432/tacbrd_pathaware";
		String user = "dbuser";
		String password = "dbuser";

		try {
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();
			rs = st.executeQuery("SELECT VERSION()");

			if (rs.next()) {
				System.out.println(rs.getString(1));
			}

			// loop through places and create placemarks
			st = con.createStatement();
			rs = st.executeQuery(query); 

			Kml kml = new Kml();
			Document document = kml.createAndSetDocument().withName("MapMiner Points of Interest");
			
			HashMap<String, String> iconStyles = new HashMap<String, String>();

			while (rs.next()) {
				// update style map
				String type = IconUtil.getPrimaryType(rs.getString("types"));
				iconStyles.put(type, IconUtil.getIconPath(type,false));
				// build geometry
				System.out.println(rs.getString("name"));
				String name = removeBadCharacters(rs.getString("name"));
				String description = "";
				description += "SourceId: " + rs.getString("source_id")	+ "<br>";
				description += "Types: " + rs.getString("types")	+ "<br>";
				description += "Vicinity: " + rs.getString("vicinity")	+ "<br>";
				description = removeBadCharacters(description);
				document.createAndAddPlacemark().withName(name)
						.withDescription(description).withOpen(Boolean.TRUE)
						.withStyleUrl("#iconStyle-" + type)
						.createAndSetPoint()
						.addToCoordinates(rs.getDouble("X"), rs.getDouble("Y"));
			}
			
			// icon styles
			for (String key : iconStyles.keySet()) {
				Style style = document.createAndAddStyle().withId("iconStyle-" + key);
				Icon icon = new Icon();
			    icon.withHref(iconStyles.get(key));
				style.createAndSetIconStyle().withIcon(icon).withScale(1);
			}

			// write out file
			kml.marshal(new File(filename));

		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			logger.error(ex.getMessage());

		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
			logger.error(ex.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				logger.error(ex.getMessage());
			}
		}
		System.out.println("End buildKML");
	}

	public static void main(String[] args) {
		buildKML("SELECT st_x(point) AS X, st_y(point) AS Y, name, source_id, types, vicinity FROM places"
				, "all.kml");
		
		// intersections
		//buildKML("SELECT st_x(places.point) AS X, st_y(places.point) AS Y, places.name AS name, places.source_id AS source_id, places.types AS types, places.vicinity AS vicinity FROM places, buildings WHERE ST_Intersects(places.point, buildings.geom)"
		//		,"https://maps.google.com/mapfiles/kml/paddle/grn-circle.png", "target/intersections.kml");
	}
}
