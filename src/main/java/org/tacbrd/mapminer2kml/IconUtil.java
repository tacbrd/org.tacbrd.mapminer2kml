package org.tacbrd.mapminer2kml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class IconUtil {
	public static String getPrimaryType(String types) {
		if (types.length() < 3) // just {}
			return "default";

		String t = types.substring(1, types.length() - 2); // trims off the
															// beginning and
															// ending braces
		t = t.replaceAll("\"", ""); // removes all double quotes
		List<String> typeList = Arrays.asList(t.split(","));
		return typeList.get(0);
		
	}
	
	public static String getIconPath(String type, boolean selected) {

		String retval = "";
		String baseurl = "https://tacboard.poznan.tacbrd.org/tacbrd-symbols/icons/HSWG/";
		// System.out.println("firstType="+firstType);
		String selectionStr = "_S1";
		if (selected)
			selectionStr = "_S3";
		
		String firstType = type;

		// default value
		retval = baseurl + "Blank_Infrastructure_B" + selectionStr + ".png";

		// used in P/A
		if (firstType.compareTo("airport") == 0)
			retval = baseurl + "Trans_Airport" + selectionStr + ".png";
		if (firstType.compareTo("bank") == 0)
			retval = baseurl + "Bank_Banks" + selectionStr + ".png";
		if (firstType.compareTo("bus_station") == 0)
			retval = baseurl + "Trans_Bus_Station" + selectionStr + ".png";
		if (firstType.compareTo("car_wash") == 0)
			retval = baseurl + "Water_Discharge_Outfall" + selectionStr
					+ ".png";
		if (firstType.compareTo("city_hall") == 0)
			retval = baseurl + "Government_Sites" + selectionStr + ".png";
		if (firstType.compareTo("courthouse") == 0)
			retval = baseurl + "Law_DOJ" + selectionStr + ".png";
		if (firstType.compareTo("embassy") == 0)
			retval = baseurl + "Government_Sites" + selectionStr + ".png";
		if (firstType.compareTo("fire_station") == 0)
			retval = baseurl + "Fire_Station" + selectionStr + ".png";
		if (firstType.compareTo("gas_station") == 0)
			retval = baseurl + "Energy_Petroleum_Facilities" + selectionStr
					+ ".png";
		if (firstType.compareTo("grocery_or_supermarket") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("hardware_store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("home_goods_store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("hospital") == 0)
			retval = baseurl + "E_Med_Hospital" + selectionStr + ".png";
		if (firstType.compareTo("local_government_office") == 0)
			retval = baseurl + "Government_Sites_Theme" + selectionStr + ".png";
		if (firstType.compareTo("lodging") == 0)
			retval = baseurl + "Edu_Schools" + selectionStr + ".png";
		if (firstType.compareTo("pharmacy") == 0)
			retval = baseurl + "E_Med_Pharmacies" + selectionStr + ".png";
		if (firstType.compareTo("police") == 0)
			retval = baseurl + "Law_Police" + selectionStr + ".png";
		if (firstType.compareTo("post_office") == 0)
			retval = baseurl + "Postal_Distribution_Center" + selectionStr
					+ ".png";
		if (firstType.compareTo("school") == 0)
			retval = baseurl + "Educational_Facilities_Theme" + selectionStr
					+ ".png";
		if (firstType.compareTo("shopping_mall") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("stadium") == 0)
			retval = baseurl + "Public_Venue_Open_Facility" + selectionStr
					+ ".png";
		if (firstType.compareTo("subway_station") == 0)
			retval = baseurl + "Trans_Tunnel" + selectionStr + ".png";
		if (firstType.compareTo("train_station") == 0)
			retval = baseurl + "Trans_Rail_Station" + selectionStr + ".png";
		if (firstType.compareTo("university") == 0)
			retval = baseurl + "Edu_College_University" + selectionStr + ".png";
		if (firstType.compareTo("veterinary_care") == 0)
			retval = baseurl + "E_Med_Medical_Facilities_Out_Patient"
					+ selectionStr + ".png";

		// not used in P/A
		if (firstType.compareTo("accounting") == 0)
			retval = baseurl + "Bank_Financial_Services_Other" + selectionStr
					+ ".png";
		if (firstType.compareTo("amusement_park") == 0)
			retval = baseurl + "Public_Venues" + selectionStr + ".png";
		if (firstType.compareTo("aquarium") == 0)
			retval = baseurl + "Water_Reservoir" + selectionStr + ".png";
		if (firstType.compareTo("art_gallery") == 0)
			retval = baseurl + "Government_Sites" + selectionStr + ".png";
		if (firstType.compareTo("atm") == 0)
			retval = baseurl + "Bank_ATMs" + selectionStr + ".png";
		if (firstType.compareTo("bakery") == 0)
			retval = baseurl + "Agri_Grain_Storage" + selectionStr + ".png";
		if (firstType.compareTo("bar") == 0)
			retval = baseurl + "Emergency_Water_Distribution_Center"
					+ selectionStr + ".png";
		if (firstType.compareTo("beauty_salon") == 0)
			retval = baseurl + "Bank_Bullion_Storage" + selectionStr + ".png";
		if (firstType.compareTo("bicycle_store") == 0)
			retval = baseurl + "Blank_Emergency_Operation_B" + selectionStr
					+ ".png";
		if (firstType.compareTo("book_store") == 0)
			retval = baseurl + "Educational_Facilities_Theme" + selectionStr
					+ ".png";
		if (firstType.compareTo("bowling_alley") == 0)
			retval = baseurl + "Blank_Emergency_Operation_A" + selectionStr
					+ ".png";
		if (firstType.compareTo("cafe") == 0)
			retval = baseurl + "Emgergency_Food_Distribution_Centers"
					+ selectionStr + ".png";
		if (firstType.compareTo("campground") == 0)
			retval = baseurl + "Emergency_Shelters" + selectionStr + ".png";
		if (firstType.compareTo("car_dealer") == 0)
			retval = baseurl + "Trans_Auto_Sales" + selectionStr + ".png";
		if (firstType.compareTo("car_rental") == 0)
			retval = baseurl + "Trans_Auto" + selectionStr + ".png";
		if (firstType.compareTo("car_repair") == 0)
			retval = baseurl + "Trans_Maintenance_Facility" + selectionStr
					+ ".png";
		if (firstType.compareTo("casino") == 0)
			retval = baseurl + "Bank_Federal_Reserve_Banks" + selectionStr
					+ ".png";
		if (firstType.compareTo("cemetery") == 0)
			retval = baseurl + "Public_Venue_Church" + selectionStr + ".png";
		if (firstType.compareTo("church") == 0)
			retval = baseurl + "Public_Venue_Religious_Institution"
					+ selectionStr + ".png";
		if (firstType.compareTo("clothing_store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("convenience_store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("dentist") == 0)
			retval = baseurl + "E_Med_Triage" + selectionStr + ".png";
		if (firstType.compareTo("department_store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("doctor") == 0)
			retval = baseurl + "E_Med_Triage" + selectionStr + ".png";
		if (firstType.compareTo("electrician") == 0)
			retval = baseurl + "Energy_Facilities" + selectionStr + ".png";
		if (firstType.compareTo("electronics_store") == 0)
			retval = baseurl + "Energy_Facilities" + selectionStr + ".png";
		if (firstType.compareTo("establishment") == 0)
			retval = baseurl + "Edu_Schools" + selectionStr + ".png";
		if (firstType.compareTo("finance") == 0)
			retval = baseurl + "Bank_Financial_Exchanges" + selectionStr
					+ ".png";
		if (firstType.compareTo("florist") == 0)
			retval = baseurl + "Agri_Grain_Storage" + selectionStr + ".png";
		if (firstType.compareTo("food") == 0)
			retval = baseurl + "Emgergency_Food_Distribution_Centers"
					+ selectionStr + ".png";
		if (firstType.compareTo("funeral_home") == 0)
			retval = baseurl + "E_Med_Morgue" + selectionStr + ".png";
		if (firstType.compareTo("furniture_store") == 0)
			retval = baseurl + "Commercial_Infrastructure_Theme" + selectionStr
					+ ".png";
		if (firstType.compareTo("general_contractor") == 0)
			retval = baseurl + "Trans_Maintenance_Facility" + selectionStr
					+ ".png";
		if (firstType.compareTo("gym") == 0)
			retval = baseurl + "Public_Venue_Enclosed_Facility" + selectionStr
					+ ".png";
		if (firstType.compareTo("hair_care") == 0)
			retval = baseurl + "Sensor_Intrusion" + selectionStr + ".png";
		if (firstType.compareTo("health") == 0)
			retval = baseurl + "E_Med_EMT_Station_Locations" + selectionStr
					+ ".png";
		if (firstType.compareTo("hindu_temple") == 0)
			retval = baseurl + "Public_Venue_Mosque" + selectionStr + ".png";
		if (firstType.compareTo("insurance_agency") == 0)
			retval = baseurl + "Bank_Financial_Exchanges" + selectionStr
					+ ".png";
		if (firstType.compareTo("jewelry_store") == 0)
			retval = baseurl + "Bank_Bullion_Storage" + selectionStr + ".png";
		if (firstType.compareTo("laundry") == 0)
			retval = baseurl + "Commercial_Infrastructure_Theme" + selectionStr
					+ ".png";
		if (firstType.compareTo("lawyer") == 0)
			retval = baseurl + "Law_DOJ" + selectionStr + ".png";
		if (firstType.compareTo("library") == 0)
			retval = baseurl + "Educational_Facilities_Theme" + selectionStr
					+ ".png";
		if (firstType.compareTo("liquor_store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("locksmith") == 0)
			retval = baseurl + "Law_Prison" + selectionStr + ".png";
		if (firstType.compareTo("meal_delivery") == 0)
			retval = baseurl + "Emgergency_Food_Distribution_Centers"
					+ selectionStr + ".png";
		if (firstType.compareTo("meal_takeaway") == 0)
			retval = baseurl + "Emgergency_Food_Distribution_Centers"
					+ selectionStr + ".png";
		if (firstType.compareTo("mosque") == 0)
			retval = baseurl + "Public_Venue_Mosque" + selectionStr + ".png";
		if (firstType.compareTo("movie_rental") == 0)
			retval = baseurl + "Telecommunications_Facility" + selectionStr
					+ ".png";
		if (firstType.compareTo("movie_theater") == 0)
			retval = baseurl + "Telecommunications_Facility" + selectionStr
					+ ".png";
		if (firstType.compareTo("moving_company") == 0)
			retval = baseurl + "Agri_Commercial_Food_Distribution_Center"
					+ selectionStr + ".png";
		if (firstType.compareTo("museum") == 0)
			retval = baseurl + "Government_Sites_Theme" + selectionStr + ".png";
		if (firstType.compareTo("night_club") == 0)
			retval = baseurl + "Edu_Schools" + selectionStr + ".png";
		if (firstType.compareTo("painter") == 0)
			retval = baseurl + "Energy_Natural_Gas_Facilities" + selectionStr
					+ ".png";
		if (firstType.compareTo("park") == 0)
			retval = baseurl + "Public_Venue_Recreational_Area" + selectionStr
					+ ".png";
		if (firstType.compareTo("parking") == 0)
			retval = baseurl + "Trans_Auto" + selectionStr + ".png";
		if (firstType.compareTo("pet_store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("physiotherapist") == 0)
			retval = baseurl + "E_Med_Medical_Facilities_Out_Patient"
					+ selectionStr + ".png";
		if (firstType.compareTo("place_of_worship") == 0)
			retval = baseurl + "Public_Venue_Church" + selectionStr + ".png";
		if (firstType.compareTo("plumber") == 0)
			retval = baseurl + "Trans_Maintenance_Facility" + selectionStr
					+ ".png";
		if (firstType.compareTo("real_estate_agency") == 0)
			retval = baseurl + "Bank_Financial_Services_Other" + selectionStr
					+ ".png";
		if (firstType.compareTo("restaurant") == 0)
			retval = baseurl + "Emgergency_Food_Distribution_Centers"
					+ selectionStr + ".png";
		if (firstType.compareTo("roofing_contractor") == 0)
			retval = baseurl + "Trans_Maintenance_Facility" + selectionStr
					+ ".png";
		if (firstType.compareTo("rv_park") == 0)
			retval = baseurl + "Trans_Bus_Station" + selectionStr + ".png";
		if (firstType.compareTo("shoe_store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("spa") == 0)
			retval = baseurl + "Water_Discharge_Outfall" + selectionStr
					+ ".png";
		if (firstType.compareTo("storage") == 0)
			retval = baseurl + "Commercial_Hazardous_Material_Storage"
					+ selectionStr + ".png";
		if (firstType.compareTo("store") == 0)
			retval = baseurl + "Agri_Food_Retail" + selectionStr + ".png";
		if (firstType.compareTo("synagogue") == 0)
			retval = baseurl + "Public_Venue_Synagogue" + selectionStr + ".png";
		if (firstType.compareTo("taxi_stand") == 0)
			retval = baseurl + "Trans_Auto_Sales" + selectionStr + ".png";
		if (firstType.compareTo("travel_agency") == 0)
			retval = baseurl + "Trans_Ship_Anchorage" + selectionStr + ".png";
		if (firstType.compareTo("zoo") == 0)
			retval = baseurl + "Agri_Animal_Feedlots" + selectionStr + ".png";

		return retval;
	}

	public static ArrayList<String> getPlaceTypesOfConcern() {
		ArrayList<String> assetTypes = new ArrayList<String>(Arrays.asList(
				"airport", "bank", "bus_station", "car_wash", "city_hall",
				"courthouse", "embassy", "fire_station", "gas_station",
				"grocery_or_supermarket", "hardware_store", "home_goods_store",
				"hospital", "local_government_office", "lodging", "pharmacy",
				"police", "post_office", "school", "shopping_mall", "stadium",
				"subway_station", "train_station", "university",
				"veterinary_care"));
		Collections.sort(assetTypes);
		return assetTypes;
	}

}
